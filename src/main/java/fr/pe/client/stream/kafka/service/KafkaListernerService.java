package fr.pe.client.stream.kafka.service;

import fr.pe.client.stream.kafka.domain.Message;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
class KafkaListenerService {

    private final Logger LOG = LoggerFactory.getLogger(KafkaListenerService.class);
    private ProcessMessageService processMessageService;

    @Autowired
    KafkaListenerService(ProcessMessageService processMessageService) {

        this.processMessageService = processMessageService;

    }

    @KafkaListener(topics = "test-topic", groupId = "500")
    void listener(Message message) throws Exception {
        LOG.info("Listener [{}]", message);
        processMessageService.processAndNotify(message);
    }

}