package fr.pe.client.stream.kafka.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Message {

    @JsonProperty("url_object_bucket")
    private String urlObjectBucket;

    @JsonProperty("id_client")
    private String idClient;

}